#!/usr/bin/env bash

##################################
##                              ##
##      © _.-= vint_243 =-._    ##
##                              ##
##################################

proxmox_git_link="https://gitlab.com/vint_243/proxmox/raw/master/pack/current"
proxmox_web_link="http://repo.godno.cc/Distrib/Proxmox/pack/current"

dev_log="/tmp/dev_install"
new_root="/mnt/os"
efi_dir="/mnt/boot/efi"

raid_name="md0"
vgname="os"
lv_boot="boot"
lv_root="root"
lv_swap="swap"
lv_tmp="tmp"
lv_var="var"

sshd_file="$new_root/etc/ssh/sshd_config"

local_image_fs="/mnt/cdrom/Proxmox/pack/current"

type_label_part(){

echo
echo "Input type partiteons label" 
echo

echo
read -p "Enter mbr/gpt : " type_label
echo

echo
echo "Type partiteons label = $type_label"
echo

}

find_disk(){

f_disk_l=`lsblk | grep "disk\|raid" | awk ' {print $1} ' | sort | uniq  |  grep -o '[a-z]\{2,3\}\|md[0-9]\{0,3\}'`

echo
echo "Find system disk "
echo

for f_d in $f_disk_l ; do parted /dev/$f_d print ; done

}

mdadm_install(){

echo
read -p "Install on RAID or next LVM install point (y/n) :  " mdadm_in
echo

if [[ $mdadm_in == "y" ]] ; then

        mdadm_create

fi

}

sys_part_gb_size(){

echo
echo "Using full disk from RAID by system "
echo

parted /dev/$sys_part_gb print | grep "GB"

echo
read -p 'Enter y/or number (from Gb by parted "-1 Gb from free") :  ' part_gb
echo

}

mdadm_create(){

raid_install="1"

echo
echo "Create RAID from mdadm"
echo

cat /dev/null > $dev_log

find_disk

echo
read -p "Enter name devices from build RAID (example sda sdb sdc ... etc) :  " raid_name_dev
echo

echo
read -p "Enter RAID Level (0,1,10 ... etc) :  " raid_level
echo

raid_num_dev=`echo $raid_name_dev | wc -w `

if [[ $type_label == "mbr" ]] ; then

	for raid_sys_disk in $raid_name_dev ; do
	
	sys_part_gb="$raid_sys_disk"

	sys_part_gb_size

	if [[ $part_gb == "y" ]] ; then
		
		echo
		echo "Using full $raid_sys_disk disk"
                echo

		parted /dev/$raid_sys_disk mklabel msdos mkpart primary ext2 0% 100%
		
	      else
		
		echo
		echo "Size parted from RAID = $part_gb"
		echo

		parted /dev/$raid_sys_disk mklabel msdos mkpart primary ext2 0% $part_gb"Gb"

	fi

	echo "/dev/"$raid_sys_disk"1 " >> $dev_log

	done
fi

if [[ $type_label == "gpt" ]] ; then
	
	for raid_sys_disk in $raid_name_dev ; do
	
	parted /dev/$raid_sys_disk mklabel gpt
	parted /dev/$raid_sys_disk mkpart primary ext2 2M 4M
	parted /dev/$raid_sys_disk set 1 bios_grub on
	parted /dev/$raid_sys_disk mkpart primary ext2 5M 290M
	sleep 5
	mkfs.vfat /dev/$raid_sys_disk"2"

	sys_part_gb="$raid_sys_disk"

	sys_part_gb_size

	if [[ $part_gb == "y" ]] ; then
		
		echo
		echo "Using full $raid_sys_disk disk"
                echo

		parted /dev/$raid_sys_disk mkpart primary ext2 300M 100%
		#sgdisk -R /dev/sdb /dev/sda
		echo "/dev/"$raid_sys_disk"3 " >> $dev_log

	     else
                
		echo
                echo "Size parted from RAID = $part_gb"
		echo

		parted /dev/$raid_sys_disk mkpart primary ext2 300M $part_gb"Gb"
                
                echo "/dev/"$raid_sys_disk"3 " >> $dev_log
	fi

	done

fi

raid_new_dev=`cat $dev_log | tr -d "\n" `

echo 
echo "Create RAID device"
echo

sleep 5

mdadm --create /dev/$raid_name --level=$raid_level  --metadata=0.9 --raid-devices=$raid_num_dev $raid_new_dev 

echo
echo "RAID device info"
echo

mdadm --detail /dev/$raid_name

}

lvm_create_one(){

lvm_install="1"

find_disk

echo
read -p "Enter system disk :  " sys_disk
echo

echo 
echo "System disk /dev/$sys_disk"
echo

echo
echo "Create label and parts"
echo

if [[ $raid_install == "1" ]] ; then
	
	parted /dev/$sys_disk mklabel msdos mkpart primary ext2 0% 100%

      else
      	
	if [[ $type_label == "mbr" ]] ; then
		
		sys_part_gb="$sys_disk"

		sys_part_gb_size

        	if [[ $part_gb == "y" ]] ; then

                	echo
			echo "Using full $sys_disk disk"
                	echo
			
			parted /dev/$sys_disk mklabel msdos mkpart primary ext2 0% 100%

		      else
		        
			echo
			echo "Size parted from RAID = $part_gb"
			echo

			parted /dev/$sys_disk mklabel msdos mkpart primary ext2 0% $part_gb"Gb"

		fi

        fi

	if [[ $type_label == "gpt" ]] ; then

        	parted /dev/$sys_disk mklabel gpt
        	parted /dev/$sys_disk mkpart primary ext2 2M 4M
        	parted /dev/$sys_disk set 1 bios_grub on
        	parted /dev/$sys_disk mkpart primary ext2 5M 290M
		sleep 5
		mkfs.vfat /dev/$sys_disk"2"

		sys_part_gb="$sys_disk"

		sys_part_gb_size

	        if [[ $part_gb == "y" ]] ; then
	
			echo
         	    	echo "Using full $sys_disk disk"
			echo

			parted /dev/$sys_disk mkpart primary ext2 300M 100%

		      else
			
			echo
		      	echo "Size parted from RAID = $part_gb"
			echo

			parted /dev/$sys_disk mkpart primary ext2 300M $part_gb"Gb"

		fi

	fi

fi

d_label=`parted /dev/$sys_disk print `

echo
echo "$d_label"
echo

echo 
echo "Create Volume Group"
echo

pv_dev=`echo $sys_disk | cut -c 1,2`

if [[ "$pv_dev" == "md" ]] ; then
        
	echo
        echo "Install from RAID"
        echo

	pvcreate /dev/$sys_disk"p1"
	vgcreate os /dev/$sys_disk"p1"
        
fi

if [[ "$pv_dev" == "sd" ]] ; then

	echo
        echo "Install from physical device"
        echo
        
	if [[ $type_label == "mbr" ]] ; then
	
		pvcreate /dev/$sys_disk"1"
	        vgcreate $vgname /dev/$sys_disk"1"
	
	fi

	if [[ $type_label == "gpt" ]] ; then
	
		pvcreate /dev/$sys_disk"3"
		vgcreate $vgname /dev/$sys_disk"3"
	
	fi

fi

}

lvm_create_many(){

lvm_install="2"

find_disk

cat /dev/null > $dev_log

echo
read -p "Enter name devices (example md0 md1 or sda sdb sdc ... etc) :   " lvm_name_dev
echo

if [[ $type_label == "mbr" ]] ; then

	for lvm_sys_disk in $lvm_name_dev ; do
        
	if [[ $type_label == "mbr" ]] ; then
		
		sys_part_gb="$lvm_sys_disk"

                sys_part_gb_size

                if [[ $part_gb == "y" ]] ; then
			
			echo
                        echo "Using full $lvm_sys_disk disk"
                        echo

			parted /dev/$lvm_sys_disk mklabel msdos mkpart primary ext2 0% 100%
			
    		     else

			echo
                        echo "Size parted from RAID = $part_gb"
                        echo

			parted /dev/$lvm_sys_disk mklabel msdos mkpart primary ext2 0% $part_gb"Gb"

		fi

	fi
	
	d_label=`parted /dev/$lvm_sys_disk print `

	echo
	echo "$d_label"

	echo 
	echo "Create Volume Group"
	echo

	pv_dev=`echo $lvm_name_dev | cut -c 1,2`

	if [[ "$pv_dev" == "md" ]] ; then
        	
		echo
	        echo "Install from RAID"
		echo

        	pvcreate /dev/$lvm_sys_disk"p1"
	        
		echo "/dev/$lvm_sys_disk"p1" " >> $dev_log

	fi

	if [[ "$pv_dev" == "sd" ]] ; then
	        
		echo
        	echo "Install from physical device"
	        echo
	        
		pvcreate /dev/"$lvm_sys_disk"1
	        
		echo "/dev/$lvm_sys_disk"1" " >> $dev_log
	fi

	done

fi

if [[ $type_label == "gpt" ]] ; then

	for lvm_sys_disk in $lvm_name_dev ; do
        parted /dev/$lvm_sys_disk mklabel gpt
        parted /dev/$lvm_sys_disk mkpart primary ext2 2M 4M
        parted /dev/$lvm_sys_disk set 1 bios_grub on
        parted /dev/$lvm_sys_disk mkpart primary ext2 5M 290M
        sleep 5
	mkfs.vfat /dev/$lvm_sys_disk"2"
	
	sys_part_gb="$lvm_sys_disk"

	sys_part_gb_size

        if [[ $part_gb == "y" ]] ; then
		
		echo
		echo "Using full $lvm_sys_disk disk"
                echo	

		parted /dev/$lvm_sys_disk mkpart primary ext2 300M 100%

   	     else
		
		 echo
                 echo "Size parted from RAID = $part_gb"
                 echo

		 parted /dev/$lvm_sys_disk mkpart primary ext2 300M $part_gb"Gb"
	fi

	d_label=`parted /dev/$lvm_sys_disk print `

        echo
        echo "$d_label"

        echo 
        echo "Create Volume Group"
        echo

        pv_dev=`echo $lvm_name_dev | cut -c 1,2`

        if [[ "$pv_dev" == "md" ]] ; then
                
		echo
                echo "Install from RAID"
                echo
		
		pvcreate /dev/$lvm_sys_disk"p1"
                
		echo "/dev/$lvm_sys_disk"p1" " >> $dev_log
	fi

        if [[ "$pv_dev" == "sd" ]] ; then
                
		echo
                echo "Install from physical device"
                echo
                
		pvcreate /dev/$lvm_sys_disk"3"
                
		echo "/dev/$lvm_sys_disk"3" " >> $dev_log
        fi

        done
fi

lvm_new_dev=`cat $dev_log | tr -d "\n" `
sleep 5
vgcreate $vgname $lvm_new_dev

}

lvm_create(){

echo
read -p "Create LVM from one device(or RAID) or union devices from Virtual Group  (y/n) :  "  lvm_union
echo

if [[ $lvm_union == "y" ]] ; then

	lvm_create_one

fi
	
if [[ $lvm_union == "n" ]] ; then

	lvm_create_many

fi

}

lvm_create_gpt(){

if [[ $type_label == "gpt" ]] ; then

	if [[ $mdadm_in == "y" ]] ; then

              mkdir -p $new_root/boot/efi

              for raid_efi_dev in $raid_name_dev ; do

              mkdir -p $efi_dir/$raid_efi_dev

              mount /dev/$raid_efi_dev"2" $efi_dir/$raid_efi_dev

              done

           else

              if [[ $lvm_union == "y" ]] ; then

                    mkdir -p $new_root/boot/efi

                    mkdir -p $efi_dir/$sys_disk

                    mount /dev/$sys_disk"2" $efi_dir/$sys_disk

              fi


              if [[ $lvm_union == "n" ]] ; then

                    mkdir -p $new_root/boot/efi

                    for lvm_efi_dev in $lvm_name_dev ; do

                    mkdir -p $efi_dir/$lvm_efi_dev

                    mount /dev/$lvm_efi_dev"2" $efi_dir/$lvm_efi_dev

                    done

              fi

	fi

fi

}

lvm_create_label(){

echo 
echo "Create system label"
echo 

echo
read -p "Create one / label [not recomend]  (y/n) :  " root_label
echo

if [[ $root_label == "y" ]] ; then
	
	echo
	vgs
	echo

	echo
	read -p "Enter root label size (Gb) : " root_label_sz
	echo
	
	echo
	echo "Size root label = $root_label_sz "
	echo

	echo
	lvcreate -n $lv_root -L "$root_label_sz"G $vgname
	echo

	echo
	vgs
	echo

	echo
	read -p "Enter swap label size (Gb) : " swap_label_sz
	echo

	echo
	echo "Size swap label = $swap_label_sz"
	echo
	
	echo
	lvcreate -n $lv_swap -L "$swap_label_sz"G $vgname 
	echo

	echo 
	echo "Create filesystem from (root,swap) parted"
	echo

	echo
	mkfs.ext4 -F /dev/$vgname/$lv_root
	mkswap /dev/$vgname/$lv_swap
	echo

	echo
	echo "Create dir from system"
	echo

	echo
	mkdir -pv $new_root
	mount /dev/$vgname/$lv_root $new_root
	echo

	lvm_create_gpt
	
	root_fstab="1"	
	
fi

if [[ $root_label == "n" ]] ; then

        
	echo
	echo "Then create label (boot,root,swap,tmp,var) "
	echo

	echo
	vgs
	echo

	echo
	read -p "Enter boot label size (Gb) : " boot_label_sz
	echo

	echo
        echo "Size boot label = $boot_label_sz"
        echo

	echo
	lvcreate -n $lv_boot -L "$boot_label_sz"G $vgname
	echo

	echo
	vgs
	echo

	echo
        read -p "Enter root label size (Gb) : "  root_label_sz
        echo

	echo
        echo "Size root label = $root_label_sz"
	echo

	echo
        lvcreate -n $lv_root -L "$root_label_sz"G $vgname
	echo

	echo
	vgs
	echo

        echo
        read -p "Enter swap label size (Gb) : "  swap_label_sz
	echo

	echo
        echo "Size swap label = $swap_label_sz"
	echo

	echo       
        lvcreate -n $lv_swap -L "$swap_label_sz"G $vgname
	echo

	echo
        vgs
	echo

	echo
        read -p "Enter tmp label size (Gb) : "  tmp_label_sz
	echo

	echo
        echo "Size tmp label = $tmp_label_sz"
	echo

	echo
        lvcreate -n $lv_tmp -L "$tmp_label_sz"G $vgname
	echo

	echo
        vgs
	echo

	echo
        read -p "Enter var label size (Gb) : "  var_label_sz
	echo

	echo
        echo "Size var label = $var_label_sz" 
	echo

	echo
        lvcreate -n $lv_var -L "$swap_label_sz"G $vgname
        echo

	echo 
	echo "Create filesystem from (boot,root,tmp,var,swap) parted"
	echo

	mkfs.ext4 -F /dev/$vgname/$lv_boot
	mkfs.ext4 -F /dev/$vgname/$lv_root
	mkfs.ext4 -F /dev/$vgname/$lv_tmp
	mkfs.ext4 -F /dev/$vgname/$lv_var
	mkswap /dev/$vgname/$lv_swap
	
	echo
        echo "Create dir from system"
	echo
        
	mkdir -pv $new_root
	mount /dev/$vgname/$lv_root $new_root
	mkdir -pv $new_root/{boot,tmp,var}
	mount /dev/$vgname/$lv_boot $new_root/boot/
	mount /dev/$vgname/$lv_tmp $new_root/tmp/
	mount /dev/$vgname/$lv_var $new_root/var
	
	echo
	lvm_create_gpt
	echo

	root_fstab="2"
	
fi

}

rescue_install(){

echo

echo
echo "Install from Rescue LiveCD"
echo

os_root="/dev/$vgname/$lv_root"
sys_label_find=`lvs | grep "$vgname" | awk ' {print $1} ' | sed "/$lv_swap\|$lv_root\|rescue/d"`
sys_label_find_num=`echo $sys_label_find | wc -w`
find_raid=`pvs | grep -o '[a-z]\{1,3\}[0-9]\{1,3\}' | grep "md" `
raid_on=`lsblk | grep "$find_raid" | head -1 | awk ' {print $6} ' | grep -o '[a-z]\{1,4\}'`

if [[ -n $os_root ]] ;then

	echo 
	echo "Root label find"
	echo
		

	root_label=`lvs | grep "$lv_root"`
	
	echo
	echo "$root_label"
	echo
	
	if [[ $raid_on == "raid" ]] ;then

                echo
                echo "Install from RAID"
                echo

                mdadm --detail /dev/$find_raid | grep -o "[svh][d][a-z]\{1\}" | xargs echo > $dev_log

            else

                echo
                echo "Install from LVM "
                echo

                pvs | grep "os" | grep -o "[svh][d][a-z]\{1\}" | xargs echo > $dev_log

        fi

        rescue_disk_install=`cat $dev_log | tr -d "\n" `

	for disk_type in $rescue_disk_install ; do

	type_label=`parted /dev/$disk_type print | awk -F":" 'NR==4' | awk ' {print $3} '`

	if [[ $type_label == "gpt" ]] ; then

		echo
		echo "This partitions label on /dev/$disk_type from gpt"
		echo
		
		mkdir -p $new_root/boot/efi/

		for efi_dev in $disk_type ; do

        	mkdir -p $efi_dir/$efi_dev
		
		sleep 2
		mkfs.vfat /dev/$efi_dev"2"
		mount /dev/$efi_dev"2" $efi_dir/$efi_dev

	        done

	fi
	
	if [[ $type_label == "msdos" ]] ; then 

		echo
                echo "This partitions label on /dev/$disk_type from mbr"
                echo

	fi

	done

	echo 
	echo "Prepare system label"
	echo

	mkfs.ext4 -F $os_root
	
	if ! [[ -d $new_root ]]; then

		mkdir -p $new_root
	
	fi

	mount $os_root $new_root

	for sys_label in $sys_label_find ; do
	
	mkdir -p $new_root/$sys_label
	mkfs.ext4 -F /dev/$vgname/$sys_label
	mount /dev/$vgname/$sys_label $new_root/$sys_label
	
	done
	
	echo
	echo "Prepare system label done"
	echo

	if [[ $sys_label_find_num == "3" ]] ; then 

		root_fstab="2"
		
	      else

	      	root_fstab="1"

	fi

     else
	
	echo
	echo "Root label not found"
	echo

	type_label_part
	mdadm_install
        lvm_create
        lvm_create_label

fi

rescue_in="1"

}

get_src(){

echo
echo "Prepare from install complate"
echo

prox_src="$new_root/tmp/proxmox"

echo 
echo "Create dir from pack Proxmox"
echo

mkdir -vp  $prox_src 

echo 
read -p "Select getting Proxmox pack from (network/local) :  " met_get 
echo

if [[ $met_get == "network" ]] ; then

	echo
	echo "Method get src from network "
	echo

	echo
	read -p "Enter mirror download (git/web) :  "  net_get
	echo

	if [[ $net_get == "git" ]] ; then

		echo
		echo "Current method download : gitlab.com "
		echo
	
		proxmox_url_link="$proxmox_git_link"

	fi

	if [[ $net_get == "web" ]] ; then

        	echo
	        echo "Current method download : web server "
        	echo

	        proxmox_url_link="$proxmox_web_link"

	fi

	echo 
	echo "Get link Proxmox pack "
	echo

	wget $proxmox_url_link -P $prox_src

	link_file="$prox_src/current"
	link_path=`cat $link_file`
	cur_link_all_num=`wc -l $link_file | awk ' {print $1} '`

	if [[ $cur_link_all_num == "1" ]] ; then

        	echo
	        echo "Current link one"
        	echo

		cur_link_rel=`echo $link_path`

	     else
		
		echo
		echo "Current link many"
	        echo

        	echo 
	        echo "List reliase"
        	echo

	        count_="0"

        	for list_rel in $link_path ; do

	        cur_link_list=`basename $list_rel`

        	let count_++

	        echo "$count_) $cur_link_list"

        	done

	        echo

        	echo
	        read -p "Enter current number reliase (1...9) :  " cur_num_rel
        	echo

	        cur_link_rel=`awk -F":" "NR==$cur_num_rel" $link_file`
        	cur_rel=`basename $cur_link_rel`

	        echo	
		echo "Current install reliase = $cur_rel "
	        echo

	fi

	echo
	echo "Download current pack Proxmox"
	echo

	wget -c $cur_link_rel{,.md5} -P $prox_src

	name_pack=`echo $cur_link_rel | xargs basename`

fi

if [[ $met_get == "local" ]] ; then
	
	echo
        echo "Method get src from local "
	echo
	
	echo
	echo "Copy current file reliase"
	echo

	cp -v $local_image_fs $prox_src

        link_path=`cat $prox_src/current`

        echo
        echo "Copy current pack Proxmox"
        echo

        cp -v $link_path{,.md5} -P $prox_src

        name_ark=`cat $prox_src/current`
        name_pack=`basename $name_ark`

fi

echo
echo "Verify checksum Proxmox pack"
echo

md5_sum=`cat $prox_src/$name_pack.md5 | awk ' {print $1} '`

echo "$md5_sum  $prox_src/$name_pack" > $prox_src/$name_pack.md5

md5_check=`md5sum -c $prox_src/$name_pack.md5 | awk ' {print $2} '`

md5_res_ru="ЦЕЛ"
md5_res_en="OK"

echo
echo "Checksum md5 verify "
echo

if [[ "$md5_check" == "$md5_res_ru" ]] ; then

       echo "Result OK"

      else
    
    	if [[ "$md5_check" == "$md5_res_en" ]] ; then

		echo "Result OK"
 
              else

		echo "Result fail"
        	
		echo
	        read -p "Ignore this fail (y/n) :  "  md5_fail
		echo 

	        if [[ $md5_fail == "y" ]] ; then
	
        	      echo 
                      echo "Ignoring this fail"
	              echo
        	      
		      echo "Next step"

                    else
            
		      echo
	              echo "Abort install"
              	      echo

		      exit 0
               
	       fi

      fi

fi

}

unpack_src(){

echo 
echo "Unpack Proxmox pack in root dir"
echo

echo
unsquashfs -f -u -d $new_root $prox_src/$name_pack
echo

efi_disk_l=`ls $efi_dir`

if [[ $type_label == "gpt" ]] ; then

	for efi_disk in $efi_disk_l ; do

	cp -vR $new_root/boot/efi/* $efi_dir/$efi_disk/	

	done

fi

echo
echo "Edit fstab from unpack OS"
echo
	
if [[ $root_fstab == "1" ]] ; then
	
      echo 
      echo "You create one root parted"
      echo

      echo "/dev/$vgname/$lv_root / ext4 errors=remount-ro 0 1	" > $new_root/etc/fstab
      echo "/dev/$vgname/$lv_swap none swap sw 0 0		" >> $new_root/etc/fstab
      echo "proc /proc proc defaults 0 0			" >> $new_root/etc/fstab
	
fi
	
if [[ $root_fstab == "2" ]] ; then	
		
      echo
      echo "You create more parted"
      echo
      
      echo "/dev/$vgname/$lv_root / ext4 errors=remount-ro 0 1	" > $new_root/etc/fstab
      echo "/dev/$vgname/$lv_boot /boot ext4 defaults 0 2	" >> $new_root/etc/fstab
      echo "/dev/$vgname/$lv_swap none swap sw 0 0		" >> $new_root/etc/fstab
      echo "/dev/$vgname/$lv_tmp /tmp ext4 defaults 0 2		" >> $new_root/etc/fstab
      echo "/dev/$vgname/$lv_var /var ext4 defaults 0 2		" >> $new_root/etc/fstab
      echo "proc /proc proc defaults 0 0			" >> $new_root/etc/fstab
			
fi

echo
echo "Fstab from unpack OS edited"
echo

}

net_conf(){

echo 
echo "Network configuration"
echo

echo
echo "Select interface from default bridge"
echo

list_iface(){

dir_cl_net="/sys/class/net"
ls_iface=`cat /proc/net/dev  | awk '{print $1}' | grep ":" | cut -d ":" -f 1`

for iface in $ls_iface;  do

if_state=`cat $dir_cl_net/$iface/operstate`

echo
echo $iface  $if_state
echo

done

}

echo
list_iface
echo

echo
read -p "Input selected interface : "  def_bridge
echo

echo
echo "Default bridge from $def_bridge interface"
echo

echo 
echo "Manual ip address from $def_bridge"
echo

echo
read -p "Input IP address : "  def_bridge_ip
echo

echo
read -p "Input network mask : "  def_bridge_mask
echo

echo
read -p "Input default route : "  def_bridge_gw
echo

echo 
read -p "Input first name server : "  def_bridge_dns1
echo

echo
read -p "Input second name server : "  def_bridge_dns2
echo

echo
read -p "Domain name : "  domain_name
echo

echo
echo "Current settings from default bridge $def_bridge"
echo

echo 
echo "IP address :  $def_bridge_ip"
echo 

echo
echo "Network mask : $def_bridge_mask "
echo

echo 
echo "Default route : $def_bridge_gw"
echo 

echo
echo "First DNS server : $def_bridge_dns1"
echo

echo 
echo "Second DNS server : $def_bridge_dns2"
echo

echo
echo "Domain name : $domain_name"
echo

echo 
echo "Tunning other interface"
echo

echo
read -p "Input y/n : " other_iface_p
echo

if [[ $other_iface_p == y ]] ; then

	echo 
	list_iface
	echo

	echo
	read -p "Input name interface : "  other_iface
	echo
	
	if [[ $def_bridge == $other_iface ]] ; then
		
		echo 
		echo "Interface $def_bridge is used"
		echo

		echo
		read -p "Input other interface : "  other_iface
		echo

		echo 
		echo "Other interface $other_iface"
		echo

	     else

		echo 
		echo "Other interface $other_iface"
		echo
	fi

	echo 
	echo "Manual ip address from $other_iface"
	echo

	echo
	read -p "Input IP address : "  other_iface_ip
	echo

	echo
	read -p "Input network mask : "  other_iface_mask
	echo

	echo
	echo "Current settings from $other_iface"
	echo

	echo 
	echo "IP address :  $other_iface_ip"
	echo 
	
	echo
	echo "Network mask : $other_iface_mask "
	echo 

fi
	
echo
echo "Writing network config file" 
echo

cat <<EOF > $new_root/etc/network/interfaces

auto lo
iface lo inet loopback
auto vmbr0
iface vmbr0 inet static
address $def_bridge_ip 
netmask $def_bridge_mask
gateway $def_bridge_gw
bridge_ports $def_bridge
bridge_stp off
bridge_fd 0
		
EOF

if [[ $other_iface_p == "y" ]] ; then
	
      	echo "					" >>  $new_root/etc/network/interfaces
	echo "auto $other_iface			" >>  $new_root/etc/network/interfaces
	echo "iface $other_iface inet static	" >>  $new_root/etc/network/interfaces
	echo "address $other_iface_ip		" >>  $new_root/etc/network/interfaces
        echo "netmask $other_iface_mask		" >>  $new_root/etc/network/interfaces
		
fi

cat << EOF > $new_root/etc/resolv.conf
nameserver $def_bridge_dns1	
nameserver $def_bridge_dns2

EOF

}

mount_serv_fs(){

echo
echo "Mounting sevice filesystem"
echo

for sv_fs in /dev/ /proc/ /run/ /sys/ ; do 

mount --bind $sv_fs $new_root/$sv_fs 

done

}

grub_install(){

echo
echo "Grub Install"
echo 

if [[ $raid_install == "1" ]] ; then 
	
	grub_dev="$raid_name_dev"

	echo 
	echo "Grub install from: $grub_dev"
	echo

fi

if [[ $lvm_install == "1" ]] ; then

	if [[ $raid_install == "1" ]] ; then
	
		echo
	     
	     else

	     	echo
	     	echo "Grub install from: $sys_disk"
  	     	echo

		grub_dev="$sys_disk"

	fi

fi

if [[ $lvm_install == "2" ]] ; then
	
	if [[ $raid_install == "1" ]] ; then

                echo
	     
	     else

		grub_dev="$lvm_name_dev"

	        echo 
	        echo "Grub install from: $grub_dev"
        	echo

	fi

fi

if [[ $rescue_in == "1" ]] ; then
	
	grub_dev="$rescue_disk_install"

	echo
	echo "Grub install from:  $grub_dev "
	echo
fi

echo
echo "Generation grub config, and write grub loader"
echo

cat <<EOF > $new_root/tmp/grub.sh
        
#!/bin/bash

grub-mkconfig -o /boot/grub/grub.cfg

EOF

for grub_install_dev in $grub_dev ; do

cat <<EOF >> $new_root/tmp/grub.sh

grub-install /dev/$grub_install_dev

EOF

done

chmod +x $new_root/tmp/grub.sh

echo
chroot $new_root/ /tmp/grub.sh
echo

}

change_pass(){

echo
echo "Change password from root user"
echo

echo
chroot $new_root passwd
echo

}

post_install(){

logo="$new_root/etc/issue"

cat << EOF > $logo

------------------------------------------------------------------------------

Welcome to the Proxmox Virtual Environment. Please use your web browser to
configure this server - connect to:

  https://$def_bridge_ip:8006/

------------------------------------------------------------------------------

EOF

} 

rename_node(){

echo
read -p "Enter name node :  "  node_name
echo

echo
echo "Name node: $node_name"
echo

echo
read -p "Enter mail administrator :  "  mail_adm
echo

echo 
echo "Mail administrator : $mail_adm"
echo

echo
echo "$node_name" > $new_root/etc/hostname
echo

cat << EOF > $new_root/etc/hosts
127.0.0.1 localhost.localdomain localhost
$def_bridge_ip $node_name.$domain_name $node_name pvelocalhost

# The following lines are desirable for IPv6 capable hosts

::1     ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
ff02::3 ip6-allhosts

EOF

path_node="$new_root/var/lib/rrdcached"
cp -avR $path_node/db/pve2-node/node1 $path_node/db/pve2-node/$node_name
rm -rf $path_node/db/pve2-node/node1
cp -avR $path_node/db/pve2-storage/node1 $path_node/db/pve2-storage/$node_name
rm -rf $path_node/db/pve2-storage/node1
rm -rf $new_root/etc/lvm/archive/* 
rm -rf $new_root/etc/lvm/backup/* 
rm -rf $path_node/journal/*
rm -rf $new_root/var/lib/pve-cluster/config.db
rm -rf $new_root/var/spool/postfix/deferred/*
cp -v $new_root/etc/{hosts,resolv.conf}  $new_root/var/spool/postfix/etc/

openssl req -new -newkey rsa:1024 -nodes -keyout $new_root/etc/pve-ssl.key -x509 -days 9999 -subj "/C=RU/ST=Arkh/L=Arkh/O=OAO/OU=Sales/CN=$node_name.$domain_name,$def_bridge_ip,127.0.0.1/emailAddress=$mail_adm (mailto:$mail_adm)" -out $new_root/etc/pve-ssl.pem

cat << EOF > $new_root/etc/postfix/main.cf
# See /usr/share/postfix/main.cf.dist for a commented, more complete version

myhostname=$node_name.$domain_name

smtpd_banner = \$myhostname ESMTP \$mail_name (Debian/GNU)
biff = no

# appending .domain is the MUA's job.
append_dot_mydomain = no

# Uncomment the next line to generate "delayed mail" warnings
#delay_warning_time = 4h

alias_maps = hash:/etc/aliases
alias_database = hash:/etc/aliases
mydestination = \$myhostname, localhost.\$mydomain, localhost
relayhost =
mynetworks = 127.0.0.0/8
inet_interfaces = loopback-only
recipient_delimiter = +

EOF

}

other_user(){

echo 
read -p "Create other user from Proxmox  (y/n) : "  other_user
echo

if [[ $other_user == "y" ]] ; then

	echo
	echo "Create other user"
	echo

	echo
	read -p "Enter name new user : "  new_user
	echo
	
	echo
	chroot $new_root useradd $new_user
	echo

	echo
	echo "Change password from new user"
	echo
	
	echo
	chroot $new_root passwd $new_user
	echo

fi

}

sshd_config_fix(){

sshd_file="$new_root/etc/ssh/sshd_config"

echo
read -p "Fix sshd_config from Proxmox  (y/n) : "   sshd_config_
echo

if [[ $sshd_config_ == "y" ]] ; then

	echo
	echo "Fixing sshd_cofig" 
	echo
	
	echo
	read -p "Change sshd port from config file (y/n) :  "  sshd_port_fix
	echo

	if [[ $sshd_port_fix == "y" ]] ; then

		sshd_port

	fi
	
	echo
        read -p "Enable permit root access from  sshd (y/n) :  "  sshd_deny_root_file
        echo

	if [[ $sshd_deny_root_file == "y" ]] ;then
		
		sshd_permit_root
	
	fi

	echo
        read -p "Accept access only other user from sshd (y/n) :  "  sshd_access_only_file
        echo

	if [[ $sshd_access_only_file == "y" ]] ; then

		sshd_allow

	fi

fi

}

sshd_port(){

echo
echo "Fixing sshd port from sshd_config"
echo

echo
read -p "Enter new port from sshd : "  sshd_new_port
echo

echo
echo "New sshd port = $sshd_new_port"
echo

echo "Port $sshd_new_port " >> $sshd_file

}

sshd_permit_root(){

echo
echo "Access root DENY"
echo

cat $sshd_file | sed '/PermitRootLogin/d' > $sshd_file"."

echo "PermitRootLogin no " >> $sshd_file"."

mv $sshd_file"." $sshd_file

}

sshd_allow(){

echo
echo "Access users"
echo

if [[ $other_user == "y" ]] ; then

	echo
	echo "Other user $new_user "
	echo

	echo
	read -p "Accept access only $new_user user (y/n) :  "  allow_user
	echo

	if [[ $allow_user == "y" ]] ; then

	     echo
	     echo "Login sshd only $new_user accept"
	     echo

	     echo "AllowUsers  $new_user" >> $sshd_file

	fi

	echo
	read -p "Deny access root user from sshd (y/n) "  deny_root
	echo

	if [[ $deny_root == "y" ]] ; then

		echo
		echo "Deny access root from sshd accept" 
		echo
		
		cat $sshd_file | sed '/DenyUsers/d' > $sshd_file"."

		echo "DenyUsers  root admin test test1 ubnt user mysql users webuser postgres  ubuntu manager  vbox pi rstudio jenkins proxmox " >> $sshd_file"."

		mv $sshd_file"." $sshd_file
	
	fi

     else

     	echo
	echo "Other users not created"
	echo

	echo
	read -p "Go to create other users (y/n) :  "  other_user_go
	echo
	
	if [[ $other_user_go == "y" ]] ; then

		other_user
		sshd_config_fix

	fi

fi

}

lvm_thin(){

echo 
echo "Create LVM Thin Provision"
echo

echo
echo "Information from used disks"
echo

find_disk

for info_disk in $f_disk_l ; do

info_vg_disk

done

echo
read -p "Enter name devices from create LVM Thin :  " name_dev 
echo

info_disk="$name_dev"

info_vg_disk

echo
read -p "Used this disk or enter other disk (y/n) :  " using_disk
echo

if [[ $using_disk == "y" ]] ; then

	echo
	read -p "Create new Volume Group or use this vg = $vg_name_ (y/n) :   " vg_new_create
	echo 

	vg_new_name_dev="$name_dev"

	if [[ $vg_new_create == "y" ]] ; then

		vg_new_create_
		
	fi
		
        echo
        echo "Used Volume Group = $vg_name_"
        echo
	
	create_lvm_thin

fi

if [[ $using_disk == "n" ]] ; then

        echo
        read -p "Enter name other devices from create LVM Thin :  " name_dev
        echo

        info_disk="$name_dev"
        info_vg_disk
	
	vg_new_name_dev="$name_dev"

	echo
        read -p "Create new Volume Group or use this vg = $vg_name_ (y/n) :   " vg_new_create
        echo

        if [[ $vg_new_create == "y" ]] ; then
	
		
                vg_new_create_
	
        fi

        echo
        echo "Used Volume Group = $vg_name_"
        echo
	
	create_lvm_thin
fi

}

info_vg_disk(){

for vg_info_disk in $info_disk ; do

pvs_head=`pvs | head -n 1`
pvs_info=`pvs | grep $vg_info_disk `
pvs_free=`echo $pvs_info | awk ' {print $6} '`
vg_name_=`echo $pvs_info | awk ' {print $2} '`

echo
echo "Print disk $vg_info_disk"
echo

if [[ -n $pvs_info ]] ; then

        echo
        echo $pvs_head
        echo
        echo $pvs_info
        echo

        echo
        echo "Free size = $pvs_free"
        echo

     else

        echo
        echo "On this disk not Volume Group"
        echo

fi

done

}

vg_new_create_(){

echo
echo "Create new Volume Group"
echo

echo
echo "Device from new Volume Group = $vg_new_name_dev"
echo

echo
echo "Create new partitions from Volume Group"
echo

echo
echo "Info from used disk"
echo

echo
parted /dev/$vg_new_name_dev print
echo

type_label=`parted /dev/$vg_new_name_dev print 2>/dev/null | awk -F":" 'NR==4' | awk ' {print $3} '`

if [[ $type_label == "msdos" ]] ; then 

	echo

     else
	
	if [[ $type_label == "gpt" ]] ; then

		echo
	     else
	     
	        type_label="create"
		
	fi

fi

start_new_part_0=`parted /dev/$name_dev print 2>/dev/null | grep "primary\|extend" | tail -1 | head -1 | awk ' {print $3} '`
start_new_part_1=`parted /dev/$name_dev print 2>/dev/null | grep "primary\|extend" | tail -2 | head -1 | awk ' {print $3} '`

if [[ -z $start_new_part_0 ]] ; then

	if [[ -z $start_new_part_1 ]] ; then

        start_new_part='0%'

	fi
	
      else

	start_new_part="$start_new_part_0"

fi

if [[ $type_label == "create" ]] ; then

	echo

	parted /dev/$name_dev mklabel gpt mkpart primary ext2 $start_new_part 100%
	
      else

	parted /dev/$name_dev mkpart primary ext2 $start_new_part 100% 

fi	

echo
echo "Create dop part done" 
echo

echo
parted /dev/$vg_new_name_dev print
echo

echo
read -p "Enter name for new Volume Group :  " new_name_vg
echo

new_part_num=`parted /dev/$name_dev print | tail -2 | head -1 | awk ' {print $1} '`

pvcreate /dev/$name_dev$new_part_num
vgcreate $new_name_vg /dev/$name_dev$new_part_num

vgs | grep $new_name_vg

vg_name_="$new_name_vg"

}

create_lvm_thin(){

echo
echo "Create LVM Thin from $vg_name_"
echo

echo
read -p "Enter name from LVM Thin :  " name_lvm_thin
echo
	
echo
echo "Name LVM Thin = $name_lvm_thin"
echo

echo
vgs
echo

echo
read -p "Enter size from LVM Thin  $name_lvm_thin (Gb) :  " size_lvm_thin
echo

lvcreate -L $size_lvm_thin"G" -T -n $name_lvm_thin $vg_name_

echo
echo "Size from LVM Thin  $name_lvm_thin = $size_lvm_thin (Gb)"
echo

echo
vgs
echo

echo
read -p "Enter size from LVM Thin meta-data (Gb) :  " size_meta_lvm_thin
echo
	
echo
echo "Size from LVM Thin meta-data $name_lvm_thin = $size_meta_lvm_thin (Gb)"
echo

lvextend -L +"$size_meta_lvm_thin"G $vg_name_/$name_lvm_thin"_tmeta"

lvs

}

clean_disk(){

os_root="/dev/$vgname/$lv_root"
find_raid=`pvs | grep "$vgname" | grep -o '[a-z]\{1,3\}[0-9]\{1,3\}' | grep "md" 2>/dev/null `
raid_on=`lsblk | grep "$find_raid" | head -1 | awk ' {print $6} ' | grep -o '[a-z]\{1,4\}' 2>/dev/null` 
raid_device_found=`mdadm --detail /dev/$find_raid | grep -o 'sd[a-z]\{1\}' 2>/dev/null ` 
lvm_device_found=`pvs | grep "os" | grep -o '[a-z]\{3\}' | grep "sd" 2>/dev/null`

echo
echo "Cleaning disk from install system"
echo

umount $new_root/{dev,proc,sys,run} > /dev/null 2>&1
umount /dev/$vgname/* > /dev/null 2>&1
rm -R $new_root > /dev/null 2>&1

umount $efi_dir/* > /dev/null 2>&1
rm -R $efi_dir > /dev/null 2>&1

if [[  $raid_on == "raid" ]] ; then

	echo 
        echo "Volume Group os found"
        echo

        echo 
        echo "Remove volume group os"
        echo

        vgremove -y $vgname

	echo
	echo "Found RAID"
	echo

	echo
	echo "Cleaning RAID $find_raid"
	echo

	dd if=/dev/zero of=/dev/$find_raid count=2048
	sleep 5

	echo 
	echo "Remove RAID $find_raid from disks"
	echo
	
	mdadm --stop /dev/$find_raid
	mdadm --remove /dev/$find_raid
	
	sleep 5

	echo $raid_device_found > $dev_log

	mdadm --zero-superblock $raid_device_found

	rm -f /dev/$find_raid

      else

        echo
        echo "Found LVM "
        echo

	echo $lvm_device_found > $dev_log

	vgremove -y $vgname

fi

device_clean_list=`cat $dev_log | tr -d "\n" `

echo
echo "Cleaning device(s) $device_clean_list"
echo

for device_clean in $device_clean_list ; do

echo
echo "Clean $device_clean device"
echo

dd if=/dev/zero of=/dev/$device_clean count=2048

sleep 5

done

echo
lsblk
echo

}

clean_path_tmp(){

echo
echo "Clean path /tmp"
echo

rm -rf $new_root/tmp/*

}

menu_install(){

while :
do
clear
echo
echo
echo -e "\t\t\t\e[1;30;1;32m  ,--------------------------------------------,\e[0m"
echo -e "\t\t\t\e[1;30;1;32m /\t\t\t\t\t\t\ \e[0m\e[0m"
echo -e "\t\t\t\e[1;30;1;32m (\e[0m\e[1;30;1;31m\t\tInstall Menu\t\t\t\e[0m\e[1;30;1;32m)\e[0m"
echo -e "\t\t\t\e[1;30;1;32m \ \t\t\t\t\t\t/\e[0m\e[0m "
echo -e "\t\t\t\e[1;30;1;32m  '--------------------------------------------'\e[0m\e[0m"
echo
echo
echo -e "\t1. Fast Install"
echo -e "\t2. Advance Install"
echo -e "\t3. Install from rescue"
echo
echo -e "\t0. Back"
echo
echo -en "\t\t" ; read -p "Please enter your choice :  " -n 1 install_opt
echo
echo

case $install_opt in
    
1)	
	echo
	echo -e "\t1. Fast Install"
	echo
	type_label_part
        lvm_create
        lvm_create_label
        get_src
        unpack_src
        net_conf
        mount_serv_fs
        grub_install
        change_pass
        post_install
        rename_node
	clean_path_tmp;;
2)	
	echo
	echo -e "\t2. Advance Install"
	echo
    	type_label_part
        mdadm_install
        lvm_create
        lvm_create_label
        get_src
        unpack_src
        net_conf
        mount_serv_fs
        grub_install
        change_pass
        other_user
        post_install
        sshd_config_fix
        rename_node
	clean_path_tmp;;
3)
	echo
	echo -e "\t3. Install from rescue"
	echo
	rescue_install
        get_src
        unpack_src
        net_conf
        mount_serv_fs
        grub_install
        change_pass
        post_install
        rename_node
	clean_path_tmp;;
0)
	break
	;;
*)
        clear

echo
echo -en "\t\tSelect menu point";;

esac

echo
echo
echo -en "\n\n\t\t\t" ; read -p "Please any key " -n 1 line
echo

done

clear

}

menu_config(){

while :
do
clear
echo
echo
echo -e "\t\t\t\e[1;30;1;32m  ,--------------------------------------------,\e[0m"
echo -e "\t\t\t\e[1;30;1;32m /\t\t\t\t\t\t\ \e[0m\e[0m"
echo -e "\t\t\t\e[1;30;1;32m (\e[0m\e[1;30;1;31m\t\tConfiguration Menu\t\t\e[0m\e[1;30;1;32m)\e[0m"
echo -e "\t\t\t\e[1;30;1;32m \ \t\t\t\t\t\t/\e[0m\e[0m "
echo -e "\t\t\t\e[1;30;1;32m  '--------------------------------------------'\e[0m\e[0m"
echo
echo
echo -e "\t1. Add other user"
echo -e "\t2. Change root password"
echo -e "\t3. Rename node"
echo -e "\t4. Net config"
echo -e "\t5. Change settings from sshd"
echo
echo -e "\t0. Back"
echo
echo -en "\t\t" ; read -p "Please enter your choice :  " -n 1 configur_opt
echo
echo

case $configur_opt in

1)
        echo
        echo -e "\t1. Add other user"
        echo
	other_user;;
2)
	echo
        echo -e "\t2. Change root password"
        echo
	change_pass;;
3)
	echo
        echo -e "\t3. Rename node"
        echo
	rename_node;;
4)
	echo
        echo -e "\t4. Net config"
        echo
	net_conf;;
5)
	echo
        echo -e "\t5. Change settings from sshd"
        echo
	sshd_config_fix;;
0)
        break
        ;;
*)
        clear

echo
echo -en "\t\tSelect menu point";;

esac

echo
echo
echo -en "\n\n\t\t\t" ; read -p "Please any key " -n 1 line
echo

done

clear

}

menu_tools(){

while :
do
clear
echo
echo
echo -e "\t\t\t\e[1;30;1;32m  ,--------------------------------------------,\e[0m"
echo -e "\t\t\t\e[1;30;1;32m /\t\t\t\t\t\t\ \e[0m\e[0m"
echo -e "\t\t\t\e[1;30;1;32m (\e[0m\e[1;30;1;31m\t\tTools Menu\t\t\t\e[0m\e[1;30;1;32m)\e[0m"
echo -e "\t\t\t\e[1;30;1;32m \ \t\t\t\t\t\t/\e[0m\e[0m "
echo -e "\t\t\t\e[1;30;1;32m  '--------------------------------------------'\e[0m\e[0m"
echo
echo
echo -e "\t1. Create LVM Thin"
echo -e "\t2. Cleaning disk from install system"
echo
echo -e "\t0. Back"
echo
echo -en "\t\t" ; read -p "Please enter your choice :  " -n 1 tools_opt
echo
echo
case $tools_opt in

1)
        echo
        echo -e "\t1. Create LVM Thin"
        echo
	lvm_thin;;
2)	
	echo
        echo -e "\t2. Cleaning disk from install system"
        echo
	clean_disk;;
0)
        break
        ;;
*)
        clear

echo
echo -en "\t\tSelect menu point";;

esac

echo
echo
echo -en "\n\n\t\t\t" ; read -p "Please any key " -n 1 line
echo

done

clear

}

while :
do
clear
echo
echo
echo -e "\t\t\t\e[1;30;1;32m  ,--------------------------------------------,\e[0m"
echo -e "\t\t\t\e[1;30;1;32m /\t\t\t\t\t\t\ \e[0m\e[0m"
echo -e "\t\t\t\e[1;30;1;32m (\e[0m\e[1;30;1;31m\tThis script installed Proxmox\t\t\e[0m\e[1;30;1;32m)\e[0m"
echo -e "\t\t\t\e[1;30;1;32m \ \t\t\t\t\t\t/\e[0m\e[0m "
echo -e "\t\t\t\e[1;30;1;32m  '--------------------------------------------'\e[0m\e[0m"
echo
echo
echo -e "\t1. Install "
echo -e "\t2. Configuration "
echo -e "\t3. Tools"
echo
echo -e "\t0. Exit"
echo
echo -en "\t\t" ; read -p "Please enter your choice :  " -n 1 main_opt
echo
echo
case $main_opt in

0)
        break;;
1)
	menu_install;;
2)
	menu_config;;
3)
	menu_tools;;
*)
        clear

echo
echo -en "\t\tSelect menu point";;

esac
echo

done

clear


#####################

##      rescue ( backup os file backup/path_info)

##      zfs

#       zram

